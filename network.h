/*
*   iLight - Arduino Version
*   v1
*
*   Created by Ricky Dall'Armellina
*   Copyright © 2017 Ricky Dall'Armellina. All rights reserved.
*
*   Network File - network.h
*
*/

// LIBRARIES
#include <SPI.h>
#include <Ethernet.h>
#include <EthernetUdp.h>

//network setup in settings.h file
// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE]; //buffer to hold incoming packet,
char  ReplyBuffer[] = "ack"; // a string to send back

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;



// FUNCTION PROTOTYPES
void start_network();
void get_command();
void parse_line(String line, String *cmd, int color[3]);

// FUNCTIONS
void start_network() {
    // start the Ethernet and UDP
    Ethernet.begin(mac, ip);
    Udp.begin(localPort);
}

void get_command() {
    // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if (packetSize) {
    Serial.print("Received packet of size ");
    Serial.println(packetSize);
    Serial.print("From ");
    IPAddress remote = Udp.remoteIP();
    for (int i = 0; i < 4; i++) {
      Serial.print(remote[i], DEC);
      if (i < 3) {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.println(Udp.remotePort());

    // read the packet into packetBufffer
    Udp.read(packetBuffer, UDP_TX_PACKET_MAX_SIZE);
    Serial.println("Contents:");
    Serial.println(packetBuffer);

    // send a reply to the IP address and port that sent us the packet we received
    Udp.beginPacket(Udp.remoteIP(), Udp.remotePort());
    Udp.write(ReplyBuffer);
    Udp.endPacket();
  }
  delay(10);
}

void parse_line(String line, String *cmd, int color[3]) {
    int i = 0; //character index in the line
    while(line[i] != divChar) {
        *cmd += line[i]; //add character to command string until found ':'
        ++i;
    }
    line = "";
    ++i; //skip divChar

    string c[3] = {"", "", ""}; //string color array
    for(int x=0; x<3; ++x) { //3 numbers to store
        for(int y=0; y<3; ++y) { //3 digits each
            c[x] += line[i];
            ++i;
        }
    }

    //write to int color array
    for(int x=0; x<3; ++x) {
        color[x] = toInt(c[x]);
    }
}