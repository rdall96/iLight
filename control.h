/*
*   iLight - Arduino Version
*   v1
*
*   Created by Ricky Dall'Armellina
*   Copyright © 2017 Ricky Dall'Armellina. All rights reserved.
*
*   Control File - control.h
*
*/

// FUNCTION DEFINITIONS
void unknown_signal();
void input_set(String *mode, int color[3]);
void render_light();

// SETTING THE CORRECT INPUT FROM CMD LINE
void input_set(String *mode, int color[3]) {
    // MODE
    if (*mode == "blackout") {
            currMode = BLACKOUT_MODE;
            isON = OFF;
        }
        else if (*mode == "whiteout") {
            currMode = WHITEOUT_MODE;
            isON = ON;
        }
        else if (*mode == "static") {
            currMode = STATIC_MODE;
            isON = ON;
        }
        else if (*mode == "rainbow") {
            currMode = RAINBOW_MODE;
            isON = ON;
        }
        else {
            unknown_signal();
        }
    
    // COLOR
    hVal = color[0];
    sVal = color[1];
    lVal = color[2];
}

void to_lights(String *cmd, int color[3]) {
    // USING CONSTANT SATURATION AND BRIGHTNESS FOR TESTING PURPOSES
    mode = *cmd;
    *cmd = "";
    input_set(&mode, color);
    render_light(); //render visuals
}


void render_light() {
    color_modes[currMode](hVal, sVal, lVal); //set the hsl to the correct value for this cycle.
    hsl_to_rgb(); //convert the HSL to RGB

    //set the colors for the pins using the converted RGB
    analogWrite(redPin, rgb[0]);
    analogWrite(greenPin, rgb[1]);
    analogWrite(bluePin, rgb[2]);
}


void unknown_signal() {
    LOG("UNKNOWN_SIGNAL\n");
}