/*
*   iLight - Arduino Version
*   v1
*
*   Created by Ricky Dall'Armellina
*   Copyright © 2017 Ricky Dall'Armellina. All rights reserved.
*
*   Main File - iLight.ino
*
*/

// INLCUDED LIBRARIES AND FILES
#include "network.h"
#include "color.h"
#include "control.h"
#include "settings.h"
 

// GLOBAL VARIABLES
int color[3]; //HSL color array
String netString = ""; //string exctracted from network
String modeCommand = ""; //command for color mode
int currMode = BLACKOUT_MODE; //refers to the current color mode (static, strobe, etc.)
String mode = "";
int hVal = 0;
int sVal = 0;
int lVal = 0;


// SETUP
void setup() {
    // set pinouts
    pinMode(redPin, OUTPUT);
    pinMode(greenPin, OUTPUT);
    pinMode(bluePin, OUTPUT);
}

// MAIN
void loop() {
    //call netowrk server to receive command
    get_command();

    //parse the command to get mode and color
    parse_line(netString, &modeCommand, color);

    //send mode and color to the lights
    to
}