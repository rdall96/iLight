/*
*   iLight - Arduino Version
*   v1
*
*   Created by Ricky Dall'Armellina
*   Copyright © 2017 Ricky Dall'Armellina. All rights reserved.
*
*   Settings File - settings.h
*
*/

// DEVELOPER OPTIONS
#define ON 1
#define OFF 0
#define LOG    ON //used to stop debug lines from printing
#define LOG(str) (LOG && Serial.print(str))

//parser divider character, mode:HSL
#define divChar ':'

//PIN DEFINITIONS
#define redPin 9   //output pins for RGB
#define greenPin 10
#define bluePin 11

// NETWORK SETUP
byte mac[] = {0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED}; //mac address for the Arduino
IPAddress ip(192, 168, 0, 150); //ip address for the Arduino
unsigned int localPort = 8888; // local port to listen on